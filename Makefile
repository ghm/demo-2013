.PHONY: capture

# - Frames capture -
# Launch the capture from a local URL
#   (file://.../demo.html?capture)
#   for AJAX same-origin-policy reasons
# Capture frames under GNU/Linux (under woe, text is not anti-aliased)
# Tested with Firefox 21
capture:
	-rm -f capture/*
	mkdir capture/
	nodejs save_capture.js

sylvain/output.webm: capture/00001.png
#	avconv -r 60 -i capture/%05d.png -r 60 -qscale 31 -y output.ogg
	avconv -r 30 -i capture/%05d.png -y sylvain/output.webm

sylvain/output.gif: capture/00001.png
	mplayer mf://capture/*.png -mf fps=30 -vo gif89a:fps=30:output=sylvain/output.gif
#	Play the animation only once
	convert sylvain/output.gif -loop 1 sylvain/output.gif
