/**
 * From the OpenGL Programming wikibook: http://en.wikibooks.org/wiki/OpenGL_Programming
 * This file is in the public domain.
 * Contributors: Sylvain Beucler
 */

// WebGL Capture - http://en.wikibooks.org/wiki/OpenGL_Programming/Video_Capture#WebGL_variant
var http = require('http');
var fs = require('fs');
http.createServer(function (req, res) {
    var idx = req.url.split('/')[1];
    var filename = "capture/" + ("0000" + idx).slice(-5)+".png";
    var img = '';
    req.on('data', function(chunk) { img += chunk });
    req.on('end', function() {
        f = fs.writeFileSync(filename, Buffer(img, 'base64'));
        res.end();
    });
    console.log('Wrote ' + filename);
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
