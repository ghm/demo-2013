// Copyright (C) 2013  Sylvain Beucler
//
// Minor changes by Luca Saiu, released into the public domain by the author,
// up to the extent possible by law
// 
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

var debug = false;
var capture = false;

var qs = document.location.href.split('?')[1];
if (qs == 'debug') debug = true;
if (qs == 'capture') capture = true;

window.onload = function() {
//    var SCREEN_WIDTH  = 960;  // window.innerWidth
//    var SCREEN_HEIGHT = 540;  // window.innerHeight
    var aspect = 960.0 / 540.0
    var height_ratio = 0.35
    var SCREEN_HEIGHT = window.innerHeight * height_ratio
    var SCREEN_WIDTH = SCREEN_HEIGHT * aspect
    while(SCREEN_WIDTH >= (window.innerWidth * 0.9)) {
      SCREEN_WIDTH *= 0.9; SCREEN_HEIGHT *= 0.9
    }
    var FOV = 45;
    var FOVrad = FOV * 3.14159/180; // radians
    var capture_ticks = 0.0;
    var capture_frame = 1;
    var scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera(FOV, aspect, 8, 800);
    var renderer;
    if (Detector.webgl) {
        // Hardware renderer
        renderer = new THREE.WebGLRenderer({ antialias: true });
    } else {
        // Software renderer
        renderer = new THREE.CanvasRenderer();
        // Don't display a warning at the bottom of gnu.org:
        // Detector.addGetWebGLMessage();
    }
    if (!window.WebGLRenderingContext) {
        // Pre-captured video
        var v = document.createElement('video');
        v.src = 'sylvain/output.webm';
        v.autoplay = true;
        document.getElementById('container').appendChild(v);
        return;
    }
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    renderer.setClearColor(0x000000, 1);
    renderer.clear();
    document.getElementById('container').appendChild(renderer.domElement);
    
    //var texture = THREE.ImageUtils.loadTexture("res_texture.png");
    //texture.anisotropy = renderer.getMaxAnisotropy();
    var font_size = 15;
    var cursor_width = 10;
    var cursor_height = 19;
    var background_color = '#000000';
    var canvas = document.createElement('canvas');
    if (debug) document.getElementById('container').appendChild(canvas);
    var text_height = font_size * 1.4;
    canvas.height = 4 * text_height;
    var g = canvas.getContext('2d');
    // Using common gnome-terminal or terminator colors:
    texts = [
        { string : 'free@IRL', color : '#00FF00', style : 'bold'},
        { string : ':',        color : '#AAAAAA', style : ''},
        { string : '~',        color : '#0000FF', style : 'bold'},
        { string : '$ ',       color : '#AAAAAA', style : ''},
        { string : 'ghm --loc europe -y 2013',
                               color : '#AAAAAA', style : ''},
    ];
    var full_size = 0;
    for (var i = 0; i < texts.length; i++) {
        t = texts[i];
        g.font = t.style + " " + font_size + "px monospace";
        full_size += g.measureText(t.string).width;
    }
    full_size += cursor_width;
    canvas.width = full_size;

    function draw_cursor(x,l) {
        if (l == undefined) l = 0;
        g.fillStyle = texts[texts.length-1].color;
        g.fillRect(Math.floor(x), Math.floor(text_height*l), cursor_width, cursor_height);
    }
    function clear_cursor(x,l) {
        if (l == undefined) l = 0;
        g.fillStyle = background_color;
        g.fillRect(Math.floor(x), Math.floor(text_height*l), cursor_width, cursor_height);
    }
    g.fillStyle = background_color;
    g.fillRect(0, 0, g.canvas.width, g.canvas.height);
    x = 0;
    for (var i = 0; i < texts.length-1; i++) {
        t = texts[i];
        g.font = t.style + " " + font_size + "px monospace";
        g.fillStyle = t.color;
        g.fillText(t.string, x, font_size);
        x += g.measureText(t.string).width;
    }
    draw_cursor(x);

    var texture = new THREE.Texture(canvas);
    texture.magFilter = THREE.NearestFilter;
    texture.needsUpdate = true;

    var geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(0,         0,         0));
    geometry.vertices.push(new THREE.Vector3(full_size, 0,         0));
    geometry.vertices.push(new THREE.Vector3(full_size, canvas.height, 0));
    geometry.vertices.push(new THREE.Vector3(0,         canvas.height, 0));
    geometry.faces.push(new THREE.Face4(0, 1, 2, 3));
    geometry.faceVertexUvs[0].push([
        new THREE.Vector2(0,0),
        new THREE.Vector2(full_size/canvas.width,0),
        new THREE.Vector2(full_size/canvas.width,1),
        new THREE.Vector2(0,1)
    ]);

    var material = new THREE.MeshBasicMaterial( {map: texture} );
    var cube = new THREE.Mesh(geometry, material);
    cube.position = new THREE.Vector3(0, 0, 0);
    scene.add(cube);

    z_one_one = (SCREEN_HEIGHT/2) / (Math.tan(FOVrad/2));  // 1:1 scale
    camera.position  = new THREE.Vector3(x, canvas.height-text_height/2, z_one_one);
    var targetCamera = new THREE.Vector3(0, canvas.height-text_height/2, 100);
    // right-align camera, leaving space for 2 characters
    // x : pixel position in the text texture
    function position_camera(x) {
        half_width = targetCamera.z * Math.tan(FOVrad/2) * (SCREEN_WIDTH/SCREEN_HEIGHT);
        targetCamera.x = (x + g.measureText('  ').width - half_width);
    }
    
    clock = new THREE.Clock();

    var strokes = [
        ['g', 0],
        ['h', 0.102],
        ['m', 0.158],
        [' ', 0.22],
        ['-', 0.329],
        ['-', 0.455],
        ['l', 0.565],
        ['o', 0.71],
        ['c', 0.828],
        [' ', 0.933],
        ['e', 1.039],
        ['u', 1.122],
        ['r', 1.236],
        ['o', 1.334],
        ['p', 1.427],
        ['e', 1.51],
        [' ', 1.658],
        ['-', 1.855],
        ['y', 2.113],
        [' ', 2.204],
        ['2', 2.43],
        ['0', 2.54],
        ['1', 2.641],
        ['3', 2.729],
    ];
    var last_char = -1;
    var wait_first = 1; // second
    var wait_after = 1; // second
    var url_drawn = false;
    function getElapsedTime() {
        if (capture) {
            return capture_ticks;
        } else {
            return clock.getElapsedTime();
        }
    }
    function render() {
        now = getElapsedTime();
        if (now > 0.5 && now < wait_first) {
            clear_cursor(x);
            texture.image = canvas;
            texture.needsUpdate = true;
        }
        for (var i = 0; i < strokes.length; i++) {
            letter = strokes[i][0];
            cur_ts = strokes[i][1] + wait_first;
            if (i > last_char && cur_ts < now) {
                clear_cursor(x);
                g.fillStyle = texts[texts.length-1].color;
                g.fillText(letter, x, font_size);
                x += g.measureText(letter).width;
                draw_cursor(x);

                last_char++;
                texture.image = canvas;
                texture.needsUpdate = true;
                position_camera(x);
            }
        }
        if (now > wait_first + strokes[strokes.length-1][1] + 0.5) {
            clear_cursor(x);
            texture.image = canvas;
            texture.needsUpdate = true;
        }
        if (!url_drawn && now > wait_first + strokes[strokes.length-1][1] + wait_after) {
            clear_cursor(x);
            //draw_cursor(0, 1);
            t = texts[texts.length-1];
            g.fillStyle = t.color;
            g.fillText('Go: IRILL (FR/Paris) - 2013/08/22-25', 0, text_height+font_size);
            g.fillText('        GNU Hackers Meeting', 0, text_height*2+font_size);
            g.fillText('       www.gnu.org/ghm/2013/', 0, text_height*3+font_size);
            texture.image = canvas;
            texture.needsUpdate = true;
            targetCamera = new THREE.Vector3(canvas.width/2, canvas.height/2, 250);
            url_drawn = true;
        }
        camera.position.x += (targetCamera.x - camera.position.x) * now / 7;
        camera.position.y += (targetCamera.y - camera.position.y) * now / 7;
        camera.position.z += (targetCamera.z - camera.position.z) * now / 7;

        var angle = now * 50;  // 10° per second
        //cube.rotation.x = (-90 + angle) / 180 * Math.PI;
        renderer.render(scene, camera);

        if (debug) stats.update();
        // WebGL Capture - http://en.wikibooks.org/wiki/OpenGL_Programming/Video_Capture#WebGL_variant
        if (capture) {
            r = XMLHttpRequest();
            r.open('POST', 'http://localhost:1337/' + capture_frame);
            r.send(renderer.domElement.toDataURL().substr("data:image/png;base64,".length));
            capture_frame++;
            capture_ticks += 1/30;
        }
        if (now < 5.0) {
            requestAnimationFrame(render);
        }
    }
    render();
}

if (debug) {
    var stats = new Stats();
    stats.setMode(0); // 0: fps, 1: ms
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';
    document.body.appendChild(stats.domElement);
}
